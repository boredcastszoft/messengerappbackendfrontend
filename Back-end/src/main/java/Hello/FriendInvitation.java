package Hello;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class FriendInvitation {
    @Id
    private String id = UUID.randomUUID().toString();
    private String sender;
    private String receiver;

    public String getId() {
        return id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }
}
