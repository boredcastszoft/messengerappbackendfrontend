package Hello;

import Hello.DAO.UserDAO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@RestController
public class LogoutRequsetHandler {
    private final UserDAO userDAO;
    private static final Logger LOGGER = Logger.getLogger(LogoutRequsetHandler.class.getName());

    @Autowired
    public LogoutRequsetHandler(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String setLogoutTimetoDB(@RequestBody User user) {
        LOGGER.info("Logout with username: " + user.getUserName());
        JSONObject obj = new JSONObject();
        obj.put("state", "ok");
        User user1 = userDAO.getByUserName(user.getUserName());
        user1.setLogOutTime(user.getLogOutTime());
        userDAO.save(user1);
        return obj.toString();
    }
}
