package Hello.DAO;

import Hello.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface FriendDAO extends JpaRepository<Friend, String> {
    List<Friend> getAllByWhoseFriend(String whoseName);

    List<Friend> findByUserFriendNameAndWhoseFriend(String userFriendName, String whoseFriend);

    void deleteByUserFriendNameAndWhoseFriend(String userFriendName, String whoseFriend);

}
