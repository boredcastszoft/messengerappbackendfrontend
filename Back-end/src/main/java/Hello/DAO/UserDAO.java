package Hello.DAO;

import Hello.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserDAO extends JpaRepository<User, String> {
    List<User> findByUserNameEquals(String userName);

    User findByUserName(String userName);

    User getByUserName(String userName);

    List<User> findAllByUserName(String userName);

    List<User> findAllByNameContains(String userName);

    void deleteByUserName(String user);

}
