package Hello.DAO;

import Hello.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface GroupDAO extends JpaRepository<Group, String> {
    Group findByGroupName(String groupName);

    List<Group> findAllByGroupName(String name);

    List<Group> findAllByGroupId(String groupId);

    void deleteByGroupName(String name);

    List<Group> findAllByUsersIsContaining(String userName);
}
