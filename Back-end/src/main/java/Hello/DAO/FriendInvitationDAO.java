package Hello.DAO;

import Hello.FriendInvitation;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface FriendInvitationDAO extends JpaRepository<FriendInvitation, String> {

    FriendInvitation getByReceiverAndSender(String receiver, String sender);

    List<FriendInvitation> getAllByReceiver(String name);

    List<FriendInvitation> getAllByReceiverAndSender(String receiver, String sender);

    void deleteByReceiverAndSender(String receiver, String sender);
}
