package Hello.DAO;

import Hello.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface MessageDAO extends JpaRepository<Message, String> {
    List<Message> getAllByReceiverAndSender(String receiver, String sender);

    List<Message> getAllBySentTimeAfterAndReceiver(Long time,String name);
    void deleteAllByReceiver(String name);
    List<Message> getAllByReceiver(String name);
}
