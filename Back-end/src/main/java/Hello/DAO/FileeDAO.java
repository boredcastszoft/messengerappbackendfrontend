package Hello.DAO;

import Hello.Filee;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface FileeDAO extends JpaRepository<Filee, String> {

}
