package Hello;


import Hello.DAO.FriendDAO;
import Hello.DAO.FriendInvitationDAO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class FriendHandler {
    private final FriendDAO friendDAO;
    private static final Logger LOGGER = Logger.getLogger(FriendHandler.class.getName());
    private final FriendInvitationDAO friendInvitationDAO;

    @Autowired
    public FriendHandler(FriendDAO friendDAO, FriendInvitationDAO friendInvitationDAO) {
        this.friendDAO = friendDAO;
        this.friendInvitationDAO = friendInvitationDAO;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/friends", method = RequestMethod.POST)
    public List<Friend> getFriendsbyname(@RequestBody User user) {
        LOGGER.info("Try to get all friend by username: " + user.getUserName());
        return friendDAO.getAllByWhoseFriend(user.getUserName());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/friendrequest", method = RequestMethod.POST)
    public String FriendRequest(@RequestBody Friend friend) {
        JSONObject obj = new JSONObject();
        LOGGER.info("Try to send friend request to " + friend.getUserFriendName() + " from " + friend.getWhoseFriend());
        if (friendInvitationDAO.getAllByReceiverAndSender(friend.getWhoseFriend(), friend.getUserFriendName()).isEmpty() && friendInvitationDAO.getAllByReceiverAndSender(friend.getUserFriendName(), friend.getWhoseFriend()).isEmpty()) {
            FriendInvitation friendInvitation = new FriendInvitation();
            friendInvitation.setSender(friend.getWhoseFriend());
            friendInvitation.setReceiver(friend.getUserFriendName());
            friendInvitationDAO.save(friendInvitation);

            obj.put("state", "ok");
            return obj.toString();
        } else {
            obj.put("state", "no");
            return obj.toString();
        }
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getfriendrequest", method = RequestMethod.POST)
    public List<FriendInvitation> getFriendRequest(@RequestBody User user) {
        LOGGER.info("Try to get friend requests by username " + user.getUserName());
        return friendInvitationDAO.getAllByReceiver(user.getUserName());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/acceptfriend", method = RequestMethod.POST)
    private String acceptFriendRequest(@RequestBody Friend friend) {
        LOGGER.info("Try to accept friend request with username :" + friend.getWhoseFriend());
        JSONObject obj = new JSONObject();
        if (friendInvitationDAO.getAllByReceiverAndSender(friend.getUserFriendName(), friend.getWhoseFriend()).isEmpty()) {
            Friend friend1 = new Friend();
            friend1.setWhoseFriend(friend.getWhoseFriend());
            friend1.setUserFriendName(friend.getUserFriendName());
            Friend friend2 = new Friend();
            friend2.setWhoseFriend(friend.getUserFriendName());
            friend2.setUserFriendName(friend.getWhoseFriend());
            friendDAO.save(friend1);
            friendDAO.save(friend2);
            friendInvitationDAO.deleteById(friendInvitationDAO.getByReceiverAndSender(friend.getWhoseFriend(), friend.getUserFriendName()).getId());
                obj.put("state", "ok");
                return obj.toString();
        } else {
            obj.put("state", "no");
            return obj.toString();
        }
    }

    @CrossOrigin("*")
    @RequestMapping(value = "/denyfriend", method = RequestMethod.POST)
    private String denyFriendRequest(@RequestBody Friend friend) {
        LOGGER.info("Try to deny friend request with " + friend.getWhoseFriend() + " from " + friend.getUserFriendName());
        JSONObject obj = new JSONObject();
        if (!friendInvitationDAO.getAllByReceiverAndSender(friend.getWhoseFriend(), friend.getUserFriendName()).isEmpty()) {
            friendInvitationDAO.deleteByReceiverAndSender(friend.getWhoseFriend(), friend.getUserFriendName());
            obj.put("state", "ok");
            return obj.toString();
        } else {
            obj.put("state", "no");
            return obj.toString();
        }
    }

    @CrossOrigin("*")
    @RequestMapping(value = "/deletefriend", method = RequestMethod.POST)
    private String deleteFriend(@RequestBody Friend friend) {
        LOGGER.info("Try to delete friend " + friend.getUserFriendName() + " with " + friend.getWhoseFriend());
        JSONObject obj = new JSONObject();
        if (!friendDAO.findByUserFriendNameAndWhoseFriend(friend.getUserFriendName(), friend.getWhoseFriend()).isEmpty() && !friendDAO.findByUserFriendNameAndWhoseFriend(friend.getWhoseFriend(), friend.getUserFriendName()).isEmpty()) {
            friendDAO.deleteByUserFriendNameAndWhoseFriend(friend.getUserFriendName(), friend.getWhoseFriend());
            friendDAO.deleteByUserFriendNameAndWhoseFriend(friend.getWhoseFriend(), friend.getUserFriendName());
            obj.put("state", "ok");
            return obj.toString();
        } else {
            obj.put("state", "no");
            return obj.toString();
        }
    }
}

