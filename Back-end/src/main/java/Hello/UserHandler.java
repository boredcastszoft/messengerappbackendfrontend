package Hello;

import Hello.DAO.UserDAO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class UserHandler {
    private final UserDAO userDAO;
    private static final Logger LOGGER = Logger.getLogger(UserHandler.class.getName());

    @Autowired
    public UserHandler(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/searchusers", method = RequestMethod.POST)
    private List<User> getUsers(@RequestBody User user) {
        LOGGER.info("Try to search users with name: " + user.getUserName());
        return userDAO.findAllByNameContains(user.getUserName());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    private String registerUser(@RequestBody User user) {
        LOGGER.info("Try to register with name: " + user.getUserName());
        JSONObject obj = new JSONObject();
        if (userDAO.findAllByUserName(user.getUserName()).isEmpty()) {
            userDAO.save(user);
            obj.put("state", "ok");
            return obj.toString();

        } else {
            obj.put("state", "no");
            return obj.toString();
        }
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/deleteuser", method = RequestMethod.POST)
    private String deleteUser(@RequestBody User user) {
        LOGGER.info("Try to delete user with name: " + user.getUserName());
        JSONObject obj = new JSONObject();
        if (!userDAO.findAllByUserName(user.getUserName()).isEmpty()) {
            userDAO.deleteByUserName(user.getUserName());
            obj.put("state", "ok");
            return obj.toString();
        } else {
            obj.put("state", "ok");
            return obj.toString();
        }
    }
}
