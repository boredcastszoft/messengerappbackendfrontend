package Hello;

import Hello.DAO.GroupDAO;
import Hello.DAO.MessageDAO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController

public class GroupHandle {
    private final GroupDAO groupDAO;
    private final MessageDAO messageDAO;
    private static final Logger LOGGER = Logger.getLogger(GroupHandle.class.getName());

    @Autowired
    public GroupHandle(GroupDAO groupDAO,MessageDAO messageDAO) {
        this.groupDAO = groupDAO;
        this.messageDAO = messageDAO;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/creategroup", method = RequestMethod.POST)
    private String createGroup(@RequestBody Group group) {
        LOGGER.info("Try to create group with name " + group.getGroupName());
        JSONObject obj = new JSONObject();
        groupDAO.save(group);
        obj.put("state", "ok");
        return obj.toString();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/deletegroup", method = RequestMethod.POST)
    private String deleteGroup(@RequestBody Group group) {
        LOGGER.info("Try to delete group with name " + group.getGroupName());
        JSONObject obj = new JSONObject();
        if (!groupDAO.findAllByGroupName(group.getGroupName()).isEmpty()) {
            groupDAO.deleteByGroupName(group.getGroupName());
            messageDAO.deleteAllByReceiver(group.getGroupName());
            obj.put("state", "ok");
            return obj.toString();
        } else {
            obj.put("state", "no");
            return obj.toString();
        }
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getgroups", method = RequestMethod.POST)
    private List<Group> getGroups(@RequestBody User user) {
        LOGGER.info("Try to get groups of " + user.getUserName());
        return groupDAO.findAllByUsersIsContaining(user.getUserName());
    }
}
