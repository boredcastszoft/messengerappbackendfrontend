package Hello;

import Hello.DAO.GroupDAO;
import Hello.DAO.MessageDAO;
import Hello.DAO.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Polling {
    private final MessageDAO messageDAO;
    private final UserDAO userDAO;
    private final GroupDAO groupDAO;
    private final ThreadPoolTaskScheduler threadPoolTaskScheduler;


    @Autowired
    public Polling(MessageDAO messageDAO, UserDAO userDAO, GroupDAO groupDAO, ThreadPoolTaskScheduler threadPoolTaskScheduler) {
        this.messageDAO = messageDAO;
        this.userDAO = userDAO;
        this.groupDAO = groupDAO;
        this.threadPoolTaskScheduler = threadPoolTaskScheduler;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/polling", method = RequestMethod.POST)
    public List<Message> pollingHandler(@RequestBody Message message) {

        Long currentTime = System.currentTimeMillis();
        if (!userDAO.findAllByUserName(message.getReceiver()).isEmpty()) {
            List<Message> list=new ArrayList<>();
            List<Group> groupList=new ArrayList<>();
            Long lastTime = userDAO.findByUserName(message.getReceiver()).getLastPollingTime();
            User user = userDAO.findByUserName(message.getReceiver());
            user.setLastPollingTime(currentTime);
            userDAO.save(user);
            int i = 0;
            while (i <= 5) {

                for(Group groups:groupDAO.findAllByUsersIsContaining(message.getReceiver())) {
                    list.addAll(messageDAO.getAllBySentTimeAfterAndReceiver(lastTime, groups.getGroupName()));
                }
                if (!messageDAO.getAllBySentTimeAfterAndReceiver(lastTime, message.getReceiver()).isEmpty() || (!list.isEmpty())) {
                    list.addAll(messageDAO.getAllBySentTimeAfterAndReceiver(lastTime, message.getReceiver()));
                    return list;
                } else {
                    i++;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }/* else if (!groupDAO.findAllByGroupName(message.getReceiver()).isEmpty()) {
            Long lastTime = groupDAO.findByGroupName(message.getReceiver()).getLastPolling();
            Group group = groupDAO.findByGroupName(message.getReceiver());
            group.setLastPolling(currentTime);
            groupDAO.save(group);
            int i = 0;
            while (i <= 5) {
                if (!messageDAO.getAllBySentTimeAfterAndReceiver(lastTime, message.getReceiver()).isEmpty()) {
                    return messageDAO.getAllBySentTimeAfterAndReceiver(lastTime, message.getReceiver());
                } else {
                    i++;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }*/
        Message msg = new Message();
        msg.setMessage("Fail");
        List<Message> fail = new ArrayList<>();
        fail.add(msg);
        return fail;
    }
}
