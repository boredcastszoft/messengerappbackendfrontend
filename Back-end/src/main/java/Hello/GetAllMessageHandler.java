package Hello;


import Hello.DAO.GroupDAO;
import Hello.DAO.MessageDAO;
import Hello.DAO.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


@RestController
public class GetAllMessageHandler {
    private final MessageDAO messageDAO;
    private final UserDAO userDAO;
    private final GroupDAO groupDAO;
    private static final Logger LOGGER = Logger.getLogger(GetAllMessageHandler.class.getName());

    @Autowired
    public GetAllMessageHandler(MessageDAO messageDAO, UserDAO userDAO, GroupDAO groupDAO) {
        this.messageDAO = messageDAO;
        this.userDAO = userDAO;
        this.groupDAO = groupDAO;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getmessages", method = RequestMethod.POST)
    public List<Message> getAllSentmessage(@RequestBody Message message) {
        LOGGER.info("Try to get messages with " + message.getReceiver());
        Long actualDate = System.nanoTime();
        User user = userDAO.getByUserName(message.getSender());
        user.setLastPollingTime(actualDate);
        userDAO.save(user);
        return returnszar(message);

    }

    private List<Message> returnszar(Message message) {
        List<Message> list = new ArrayList<>();
        if(!groupDAO.findAllByGroupName(message.getReceiver()).isEmpty()){
            list.addAll(messageDAO.getAllByReceiver(message.getReceiver()));
        }
        else {
            list.addAll(messageDAO.getAllByReceiverAndSender(message.getReceiver(), message.getSender()));
            list.addAll(messageDAO.getAllByReceiverAndSender(message.getSender(), message.getReceiver()));
        }
        /*for(Message asd:messageDAO.getAllByReceiver(message.getReceiver())){
            System.out.println("FASZOM "+asd);
        }*/
        return list;
    }
}
