package Hello;

import Hello.DAO.UserDAO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@RestController
public class LoginRequestHandler {
    private final UserDAO userDAO;
    private static final Logger LOGGER = Logger.getLogger(LoginRequestHandler.class.getName());

    @Autowired
    public LoginRequestHandler(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    private String loginmessage;

    public void setLoginmessage(String loginmessage) {
        this.loginmessage = loginmessage;
    }


    public void checkUservalidation(User user) {
        JSONObject obj = new JSONObject();
        if (userDAO.findAllByUserName(user.getUserName()).isEmpty()) {
            obj.put("state", "wus");
        } else if (!userDAO.findByUserNameEquals(user.getUserName()).get(0).getPassword().equals(user.getPassword())) {
            obj.put("state", "nop");
        } else {
            obj.put("state", "ok");
            obj.put("id", user.getUserName());
        }
        setLoginmessage(obj.toString());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String getLoginData(@RequestBody User user) {
        LOGGER.info("Try to login with username: " + user.getUserName());
        checkUservalidation(user);
        return loginmessage;
    }

}