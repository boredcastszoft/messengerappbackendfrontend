package Hello;

import Hello.DAO.MessageDAO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@RestController
public class MessageSenderHandler {
    private final MessageDAO messageDAO;
    private static final Logger LOGGER = Logger.getLogger(MessageSenderHandler.class.getName());

    @Autowired
    public MessageSenderHandler(MessageDAO messageDAO) {
        this.messageDAO = messageDAO;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public String getSentmessage(@RequestBody Message message) {
        LOGGER.info("Put sent message to DB: " + message.getMessage());
        messageDAO.save(message);
        JSONObject obj = new JSONObject();
        obj.put("state", "ok");
        return obj.toString();
    }
}
