package Hello;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Friend {
    @Id
    private String friendId = UUID.randomUUID().toString();
    private String userFriendName;
    private String whoseFriend;


    public String getUserFriendName() {
        return userFriendName;
    }

    public void setUserFriendName(String userFriendName) {
        this.userFriendName = userFriendName;
    }

    public String getWhoseFriend() {
        return whoseFriend;
    }

    public void setWhoseFriend(String whoseFriend) {
        this.whoseFriend = whoseFriend;
    }

    public String getFriendId() {
        return friendId;
    }
}
