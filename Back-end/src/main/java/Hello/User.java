package Hello;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class User  {
    @Id
    private String userId = UUID.randomUUID().toString();
    private String userName;
    private String name;
    private Boolean online;
    private Long logOutTime;
    private String password;
    private Long lastPollingTime;

    public User() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public Long getLogOutTime() {
        return logOutTime;
    }

    public void setLogOutTime(Long logOutTime) {
        this.logOutTime = logOutTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getLastPollingTime() {
        return lastPollingTime;
    }

    public void setLastPollingTime(Long lastPollingTime) {
        this.lastPollingTime = lastPollingTime;
    }
}
