package Hello;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Groups")
public class Group {
    @Id
    private String groupId = UUID.randomUUID().toString();
    private String groupName;
    @Lob
    private String users;
    private Long lastPolling;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public Long getLastPolling() {
        return lastPolling;
    }

    public void setLastPolling(Long lastPolling) {
        this.lastPolling = lastPolling;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }
}
