package Hello;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
public class ThreadPool {
    @Bean
    public ThreadPoolTaskScheduler pollingThread() {
        ThreadPoolTaskScheduler pollingThread = new ThreadPoolTaskScheduler();
        pollingThread.setPoolSize(5);
        pollingThread.setThreadNamePrefix("default-polling-thread");
        return pollingThread;
    }
}
