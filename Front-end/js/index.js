var Username;
var serurl="http://172.21.28.55:8182";

$('.form').find('input').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

$("#lin").on('click', function (e){
    e.preventDefault();

    var urlk = serurl + "/login";
   // console.log(urlk);
    var username=$("#mail").val();
    var data = JSON.stringify({"userName": $("#mail").val(), "password": $("#pw").val()});
    $.postJSON = function(url, data) {
        return jQuery.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'type': 'POST',
            'url': url,
            'data': data,
            'dataType': 'json',
            success: function( data){
                //var json = JSON.parse(data);
                //console.log(data);
                if(data.state == "wus"){
                    var er=$("<p class='R'>Nincs ilyen felhasználó !</p>");
                    $("#errl1 .R").remove();
                    $("#errl1").append(er);
                }else if(data.state == "nop"){
                    var er=$("<p class='R'>Hibás jelszó !</p>");
                    $("#errl2 .R").remove();
                    $("#errl2").append(er);
                }else if(data.state == "ok"){
                    //Username=data.userName;
                    localStorage.TUsername = username;
                    console.log("login : "+localStorage.TUsername);
                    window.location.replace("./Chat.html");
                }else{
                    //hibakez
                }
            },
        });
    };
    $.postJSON(urlk, data);
});

$("#reg").on('click', function (e){
    e.preventDefault();

    var urlk = serurl + "/register";
    //console.log(urlk);

    var data = JSON.stringify({"userName": $("#un").val(),"name": $("#name").val(), "password": $("#spw").val()});
    $.postJSON = function(url, data) {
        return jQuery.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'type': 'POST',
            'url': url,
            'data': data,
            'dataType': 'json',
            success: function( data){
                //var json = JSON.parse(data);
                //console.log(data);
                if(data.state == "ok"){
                    var er=$("<p class='G'>Sikeres regisztráció !</p>");
                    $("#ree").append(er);
                }else if(data.state == "not ok"){
                    var er=$("<p class='R'>Nem sikerült a regisztráció !</p>");
                    $("#ree").append(er);
                }else {

                }
            },
        });
    };
    $.postJSON(urlk, data);
});